package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
)

func main() {

	url := os.Getenv("OMUP_ENVS_URL")
	filePath := os.Getenv("OMUP_FILE_PATH")

	// Do the request
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("receving response %v", resp)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	log.Printf("received response: %s", string(body))

	// Delete the file if exists
	if fileExists(filePath) {
		err := os.Remove(filePath)
		if err != nil {
			log.Fatalf("deleting the file %v", err)
		}
	}

	// Create new file
	f, err := os.Create(filePath)
	if err != nil {
		log.Fatalf("creating the file %v", err)
	}
	defer f.Close()

	// Make the file executable
	err, out := execCommand("chmod", "+x", filePath)
	if err != nil {
		log.Fatalf("making the file: %s executable, error: %v", filePath, err)
	}
	log.Println(out.String())

	_, err = f.Write(body)
	if err != nil {
		log.Fatalf("writing in file: %s, error: %v", filePath, err)
	}

	// Run the file
	err, out = execCommand("sh", filePath)
	if err != nil {
		log.Fatalf("execute command %v", err)
	}
	log.Println(out.String())
}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// execCommand execute an terminal command, if something go wrong it
// will return an error.
func execCommand(name string, arg ...string) (error, bytes.Buffer) {
	cmd := exec.Command(name, arg...)
	var out bytes.Buffer
	cmd.Stdout = &out
	return cmd.Run(), out
}
